import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from 'src/app/components/header/header.module';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { InputModule } from '@pacifish/pacifish-lib';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    LoginComponent,
    LandingComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputModule,
    HeaderModule
  ]
})
export class UnsecuredModule { }
