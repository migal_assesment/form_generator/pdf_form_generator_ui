import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  formGroup: FormGroup;
  failedLoginMessage;
  loginSubscription!: Subscription;

  constructor(
      private router: Router,
      private authService: AuthService
     ) { 
    this.formGroup = this.createFormGroup();
    this.failedLoginMessage = '';
  }

  ngOnInit(): void {
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      login:new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)])
    });
  }

  loginViaEmailAndPassword() {
    if(this.formGroup.valid) {
      const login: string = this.formGroup.get('login')?.value;
      const password: string = this.formGroup.get('password')?.value;
      this.loginSubscription = this.authService.login(login,password).subscribe(data => {
        this.router.navigate(['home']);
      });
    } else {
      this.failedLoginMessage = 'email or password is invalid';
    }

  }

  ngOnDestroy(): void {
    if(this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }

}
