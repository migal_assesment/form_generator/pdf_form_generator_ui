import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { MockAuthService } from 'src/app/services/auth.mock.service';
import { AuthService } from 'src/app/services/auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';


describe('LoginComponent', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockAuthService: MockAuthService;
  let router: Router;

  beforeEach(async () => {
    mockAuthService = new MockAuthService();

    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ ReactiveFormsModule,
        RouterTestingModule,
         FormsModule ],
      providers: [
        { provide: AuthService, useValue: mockAuthService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.formGroup.valid).toBeFalsy();
  });

  it('login button should be disabled when form is invalid', () => {
    component.formGroup.controls['login'].setValue('');
    component.formGroup.controls['password'].setValue('');
    expect(component.formGroup.valid).toBeFalsy();
  });

  it('should display failedLoginMessage when form is invalid on login attempt', () => {
    component.loginViaEmailAndPassword();
    expect(component.failedLoginMessage).toEqual('email or password is invalid');
  });

  it('should navigate to home on successful login', () => {
    const loginValue = 'test@example.com';
    const passwordValue = '12345';
    mockAuthService.login.and.returnValue(of({})); 
    spyOn(router, 'navigate');

    component.formGroup.controls['login'].setValue(loginValue);
    component.formGroup.controls['password'].setValue(passwordValue);
    component.loginViaEmailAndPassword();

    expect(mockAuthService.login).toHaveBeenCalledWith(loginValue, passwordValue);
    expect(router.navigate).toHaveBeenCalledWith(['home']);
  });


});
