import { Component, OnInit } from '@angular/core';
import { Performance, PerformanceComparator } from 'src/app/models/performance.model';
import { AuthService } from 'src/app/services/auth.service';
import { FormService } from 'src/app/services/form.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  performanceComparator!: PerformanceComparator;
  showResults: boolean;

  constructor(private authService: AuthService,
              private formService: FormService) { 
                this.showResults = false;
              }

  ngOnInit(): void {
    
  }

  onLogout() {
    this.authService.logout();
  }

  onCreateReport() {
    this.formService.createForms().subscribe(data => {
      this.showResults = true;
      this.performanceComparator = data;
    });
  }



}
