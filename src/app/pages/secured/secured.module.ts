import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HeaderModule } from 'src/app/components/header/header.module';
import { PerformanceResultsModule } from 'src/app/components/performance-results/performance-results.module';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    PerformanceResultsModule
  ]
})
export class SecuredModule { }
