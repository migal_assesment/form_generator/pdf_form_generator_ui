import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { PerformanceComparator } from '../models/performance.model';

@Injectable({
  providedIn: 'root'
})
export class FormService {


  private createFormsUrl = environment.microServices.pdfFormGenerator.url+
                      environment.microServices.pdfFormGenerator.context +
                      '/api/files/createForms'; 


  constructor(private http: HttpClient, 
              private authService: AuthService) { }

  createForms(): Observable<PerformanceComparator> {
    let headers: HttpHeaders = new HttpHeaders();
    if(this.authService.getToken() != null){
      headers = headers.set('Authorization','Bearer '+ this.authService.getToken());
    }
    console.log(headers);
    return this.http.post<PerformanceComparator>(this.createFormsUrl, null, {headers});
  }

}

