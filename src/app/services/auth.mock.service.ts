export class MockAuthService {
    logout = jasmine.createSpy('logout');

    login = jasmine.createSpy('login');

    setToken = jasmine.createSpy('setToken');

    getToken = jasmine.createSpy('getToken');
}