import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PerformanceService {
  private performanceStreamUrl = environment.microServices.pdfFormGenerator.url+
  environment.microServices.pdfFormGenerator.context +
  '/api/performance/stream'; 

  constructor() { }

  getPerformanceLogs(): Observable<any> {
    return new Observable(observer => {
      const eventSource = new EventSource(this.performanceStreamUrl);

      eventSource.onmessage = event => {
        const json = JSON.parse(event.data);
        observer.next(json);
      };

      eventSource.onerror = error => {
        if (eventSource.readyState === 0) {
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      };

      return () => {
        eventSource.close();
      };
    });
  }
}
