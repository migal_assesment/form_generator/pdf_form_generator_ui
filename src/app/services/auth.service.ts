import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private loginUrl = environment.microServices.pdfFormGenerator.url+
                      environment.microServices.pdfFormGenerator.context +
                      '/auth/login'; 

  private logoutUrl = environment.microServices.pdfFormGenerator.url+
                      environment.microServices.pdfFormGenerator.context +
                      '/auth/logout'; 

  constructor(private http: HttpClient) { }

  login(login: string, password: string): Observable<User> {
    return this.http.post<User>(this.loginUrl, {login, password}).pipe(
      map((user: User)=> {
        this.setToken(user.token);
        return user;
      }));
  }

  logout() {
    return this.http.post(this.logoutUrl, {});
  }

  setToken(token: string | null): void {
    if(token) {
      window.localStorage.setItem('auth_token', token);
    } else {
      window.localStorage.removeItem('auth_token');
    }
  }

  getToken() {
    return window.localStorage.getItem('auth_token');
  }
}

