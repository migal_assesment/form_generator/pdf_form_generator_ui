import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() showLogin: boolean;
  @Output() logout: EventEmitter<boolean>;
  activeClass: string;
  collapseNav: boolean;
  home: string;



  constructor(private router: Router) { 
    this.showLogin = true;
    this.activeClass = 'home';
    this.collapseNav = false;
    this.home = 'landing';
    this.logout = new EventEmitter();
  }

  ngOnInit(): void {
  }

  onNavigate(nav: string) {
    this.router.navigate([nav]);
    this.activeClass = nav;
  }

  openCollapseNav() {
    this.collapseNav = !this.collapseNav;
  }

  onLogout() {
    this.logout.emit(true);
  }

}
