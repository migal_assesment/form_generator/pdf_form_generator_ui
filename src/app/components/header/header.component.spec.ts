import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    router = TestBed.inject(Router)
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('When openCollapseNav is called', () => {

    describe('And collapseNav is false', () => {
      it('should create', () => {
        component.collapseNav = false;
        component.openCollapseNav()
        expect(component.collapseNav).toEqual(true);
      });
    });

    describe('And collapseNav is true', () => {
      it('should create', () => {
        component.collapseNav = true;
        component.openCollapseNav()
        expect(component.collapseNav).toEqual(false);
      });
    });

  });

  describe('When onNavigate is called', () => {

    beforeEach(() => {
      spyOn(router, 'navigate');
      component.onNavigate('home');
    });

    describe('and nav is called with home', () => {
      it('should create', () => {
        expect(router.navigate).toHaveBeenCalledWith(['home']);
      });

      it('should set activeClass to home', () => {
        expect(component.activeClass).toEqual('home');
      });
    });

  });
});
