import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerformanceResultsComponent } from './performance-results.component';



@NgModule({
  declarations: [
    PerformanceResultsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PerformanceResultsComponent
  ],
})
export class PerformanceResultsModule { }
