import { Component, Input, OnInit } from '@angular/core';
import { Performance } from 'src/app/models/performance.model';

@Component({
  selector: 'app-performance-results',
  templateUrl: './performance-results.component.html',
  styleUrls: ['./performance-results.component.scss']
})
export class PerformanceResultsComponent implements OnInit {
  @Input() performance!: Performance;
  @Input() primary: boolean;

  constructor() { 
    this.primary = true;
  }

  ngOnInit(): void {
  }

}
