export interface Performance {
    methodName: string;
    serviceName: string;
    durationNano: number;
    winner: boolean;
    durationDateTime?: Date;
}

export interface PerformanceComparator {
    s3: Performance;
    fileServer: Performance;
}