import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UnsecuredModule } from './pages/unsecured/unsecured.module';
import { SecuredModule } from './pages/secured/secured.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    UnsecuredModule,
    SecuredModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
