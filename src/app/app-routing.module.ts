import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/unsecured/login/login.component';
import { LandingComponent } from './pages/unsecured/landing/landing.component';
import { HomeComponent } from './pages/secured/home/home.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing'
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { 
      animation: 1
    }
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { 
      animation: 1
    }
  },
  {
    path: 'landing',
    component: LandingComponent, 
    data: { 
      animation: 0
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
